/*
 * Copyright (c) Jonas Aaberg <cja@lithops.se> 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <pebble.h>

#ifdef PBL_PLATFORM_BASALT
    #define NUM_WEEKS 6
    #define TITLE_OFFSET_X 0
    #define TITLE_OFFSET_Y 0
    #define TITLE_HEIGHT 20
    #define CAL_START_OFFSET_X 0
#endif

#ifdef PBL_PLATFORM_CHALK
    #define NUM_WEEKS 5
    #define TITLE_OFFSET_X 14
    #define TITLE_OFFSET_Y 5
    #define TITLE_HEIGHT 20
    #define CAL_START_OFFSET_X 18
#endif

#define NUM_DAYS 7

#define KALENDER_TIMEOUT 60*1000 /* ms */

enum dayevent {
	DAY_NORMAL     = 0,
	DAY_RED        = (1 << 0),
	DAY_OFF        = (1 << 1),
	DAY_MONTH1     = (1 << 2),
	DAY_MONTH2     = (1 << 3),
	DAY_MONTH3     = (1 << 4),
	DAY_CAKE       = (1 << 5),
	DAY_SEMLA      = (1 << 6),
	DAY_PI         = (1 << 7),
	DAY_HEART      = (1 << 8),
	DAY_BOOK       = (1 << 9),
	DAY_VALBORG    = (1 << 10),
	DAY_WAFFLE     = (1 << 11),
	DAY_KANELBULLE = (1 << 12),
	DAY_EASTER_EGG = (1 << 13),
	DAY_MIDSUMMER  = (1 << 14),
	DAY_SWEDEN     = (1 << 15),
	DAY_LUCIA      = (1 << 16),
	DAY_CRAWFISH   = (1 << 17),
	DAY_SANTA      = (1 << 18),
	DAY_FIREWORKS  = (1 << 19),

	DAY_TODAY      = (1 << 20),
};

struct day {
	enum dayevent flag;
	char day;
};

struct cal_view {
	char start_month, end_month, curr_month;
	short year;
	char weeks[NUM_WEEKS];
	struct day days[NUM_WEEKS*NUM_DAYS];
};

struct special_day {
	char day;
	char mon;
	enum dayevent event;
};

struct easterday {
	char day;
	char mon;
};

struct day2img {
	enum dayevent event;
	uint32_t res;
};

static struct cal_view s_view;
static time_t s_now;

static Window *s_window;
static Layer *s_root_layer;
static char s_today_day, s_today_mon;
static AppTimer *s_timer = NULL;

static const struct special_day special_days[] = {
	{ 1,  1, DAY_RED},
	{ 2,  1, DAY_CAKE},
	{ 5,  1, DAY_OFF},
	{ 6,  1, DAY_RED},
	{10,  1, DAY_CAKE},
	{ 5,  2, DAY_CAKE},
	{10,  3, DAY_CAKE},
	{11,  3, DAY_CAKE},
	{14,  3, DAY_PI},
	{25,  3, DAY_WAFFLE},
	{22,  4, DAY_HEART},
	{23,  4, DAY_BOOK},
	{30,  4, DAY_VALBORG},
	{ 1,  5, DAY_RED},
	{ 6,  6, DAY_SWEDEN},
	{12,  6, DAY_CAKE},
	{21,  7, DAY_HEART},
	{ 4, 10, DAY_KANELBULLE},
	{10, 10, DAY_CAKE},
	{12, 10, DAY_CAKE},
	{13, 12, DAY_LUCIA},
	{24, 12, DAY_SANTA},
	{25, 12, DAY_RED},
	{26, 12, DAY_RED},
	{31, 12, DAY_FIREWORKS},
	{ 0,  0, DAY_NORMAL}, /* End marker */
};

static const struct day2img day2img[] = {
	{DAY_CAKE,       RESOURCE_ID_IMAGE_CAKE},
	{DAY_SEMLA,      RESOURCE_ID_IMAGE_SEMLA},
	{DAY_PI,         RESOURCE_ID_IMAGE_PI},
	{DAY_HEART,      RESOURCE_ID_IMAGE_HEART},
	{DAY_BOOK,       RESOURCE_ID_IMAGE_BOOK},
	{DAY_VALBORG,    RESOURCE_ID_IMAGE_VALBORG},
	{DAY_WAFFLE,     RESOURCE_ID_IMAGE_WAFFLE},
	{DAY_KANELBULLE, RESOURCE_ID_IMAGE_KANELBULLE},
	{DAY_EASTER_EGG, RESOURCE_ID_IMAGE_EASTER_EGG},
	{DAY_MIDSUMMER,  RESOURCE_ID_IMAGE_MIDSUMMER},
	{DAY_SWEDEN,     RESOURCE_ID_IMAGE_SWEDEN},
	{DAY_LUCIA,      RESOURCE_ID_IMAGE_LUCIA},
	{DAY_CRAWFISH,   RESOURCE_ID_IMAGE_CRAWFISH},
	{DAY_SANTA,      RESOURCE_ID_IMAGE_SANTA},
	{DAY_FIREWORKS,  RESOURCE_ID_IMAGE_FIREWORKS},
	{DAY_NORMAL, 0},
};

/* Easter day, month from 2000+ Enough years until we're dead */
static const struct easterday easter[] = {
	{ 23, 4},
	{ 15, 4},
	{ 31, 3},
	{ 20, 4},
	{ 11, 4},
	{ 27, 3},
	{ 16, 4},
	{  8, 4},
	{ 23, 3},
	{ 12, 4},
	{  4, 4},
	{ 24, 4},
	{  8, 4},
	{ 31, 3},
	{ 20, 4},
	{  5, 4},
	{ 27, 3},
	{ 16, 4},
	{  1, 4},
	{ 21, 4},
	{ 12, 4},
	{  4, 4},
	{ 17, 4},
	{  9, 4},
	{ 31, 3},
	{ 20, 4},
	{  5, 4},
	{ 28, 3},
	{ 16, 4},
	{  1, 4},
	{ 21, 4},
	{ 13, 4},
	{ 28, 3},
	{ 17, 4},
	{  9, 4},
	{ 25, 3},
	{ 13, 4},
	{  5, 4},
	{ 25, 4},
	{ 10, 4},
	{  1, 4},
	{ 21, 4},
	{  6, 4},
	{ 29, 3},
	{ 17, 4},
	{  9, 4},
	{ 25, 3},
	{ 14, 4},
	{  5, 4},
	{ 18, 4},
	{ 10, 4},
	{  2, 4},
	{ 21, 4},
	{  6, 4},
	{ 29, 3},
	{ 18, 4},
	{  2, 4},
	{ 22, 4},
	{ 14, 4},
	{ 30, 3},
	{ 18, 4},
	{ 10, 4},
	{ 26, 3},
	{ 15, 4},
	{  6, 4},
	{ 29, 3},
	{ 11, 4},
	{  3, 4},
	{ 22, 4},
	{ 14, 4},
	{ 30, 3},
	{ 19, 4},
	{ 10, 4},
	{ 26, 3},
	{ 15, 4},
};

static bool is_easter_delta(struct tm *t, int delta)
{
	time_t d;
	struct tm old;

	memcpy(&old, t, sizeof(struct tm));

	t->tm_mday = easter[t->tm_year - 100].day;
	t->tm_mon = easter[t->tm_year - 100].mon - 1;

	d = mktime(t);
	d += delta * 24*60*60;
	t = localtime(&d);

	bool h = (t->tm_mon == old.tm_mon && t->tm_mday == old.tm_mday);
	memcpy(t, &old, sizeof(struct tm));

	return h;
}

static bool is_holiday(struct tm *t)
{
	if (is_easter_delta(t, -2))
		return true;

	if (is_easter_delta(t, 1))
		return true;

	if (is_easter_delta(t, 39))
		return true;

	/* Midsummers day */
	if (t->tm_wday == 6 && t->tm_mon == 5 && t->tm_mday >= 20 && t->tm_mday <= 26)
		return true;

	/* Alla helgons dag */
	if (t->tm_wday == 6 && ((t->tm_mon == 9 && t->tm_mday == 31) || (t->tm_mon == 10 && t->tm_mday <= 6)))
		return true;

	return false;
}

#define WEEK_BUFF_LEN 8
static void generate_view(struct cal_view *v, time_t today, int week_center)
{
	struct tm *t;
	int d;
	time_t start;
	int w_idx = 0;
	char week_buf[WEEK_BUFF_LEN];
	enum dayevent month_color = DAY_MONTH1;

	t = localtime(&today);
	v->curr_month = t->tm_mon;
	v->year = t->tm_year + 1900;

	if (t->tm_wday == 0)
		d = 6;
	else
		d = t->tm_wday - 1;
	d += week_center * 7;

	start = today - d*24*60*60;

	t = localtime(&start);
	v->start_month = t->tm_mon;

	for (int i = 0; i < (NUM_WEEKS*NUM_DAYS); i++) {
		time_t day = start + 24*60*60*i;
		t = localtime(&day);

		if (t->tm_wday == 1) {
			strftime(week_buf, WEEK_BUFF_LEN, "%V", t);
			v->weeks[w_idx] = atoi(week_buf);
			w_idx++;
		}

		v->days[i].day = t->tm_mday;
		v->days[i].flag = DAY_NORMAL;

		if (t->tm_mday == 1 && t->tm_mon != v->start_month)
			month_color = month_color << 1;

		v->days[i].flag |= month_color;

		if (t->tm_wday == 0 || is_holiday(t))
			v->days[i].flag |= DAY_RED;
		else if (t->tm_wday == 6)
			v->days[i].flag |= DAY_OFF;

		for (int j = 0; special_days[j].day != DAY_NORMAL; j++) {
			if (special_days[j].day == t->tm_mday &&
			    special_days[j].mon == t->tm_mon + 1) {
				v->days[i].flag |= special_days[j].event;
				break;
			}
		}

		/* Midsummers eve */
		if (t->tm_wday == 5 && t->tm_mon == 5 && t->tm_mday >= 19 && t->tm_mday <= 25)
			v->days[i].flag |= DAY_MIDSUMMER;

		/* Kraftpremiar! */
		if (t->tm_wday == 3 && t->tm_mon == 7 && t->tm_mday >= 1 && t->tm_mday <= 7)
			v->days[i].flag |= DAY_CRAWFISH;

		if (is_easter_delta(t, 0))
			v->days[i].flag |= DAY_EASTER_EGG;

		/* Fettisdagen */
		if (is_easter_delta(t, -47))
			v->days[i].flag |= DAY_SEMLA;

		if (t->tm_mon == s_today_mon && t->tm_mday == s_today_day)
			v->days[i].flag |= DAY_TODAY;
	}

	v->end_month = t->tm_mon;
}

static void kalender_timeout_cb(void *data)
{
	(void) window_stack_pop(false);
}

static void kalender_timer_restart(void)
{
	if (s_timer)
		app_timer_reschedule(s_timer, KALENDER_TIMEOUT);
	else
		s_timer = app_timer_register(KALENDER_TIMEOUT, kalender_timeout_cb, NULL);
}

#define NUM_BUF_LEN 8
#define TITLE_BUF_LEN 40
static void kalender_update_proc(Layer *layer, GContext *ctx)
{
	static const char *mname[] = {"Januari", "Februari", "Mars", "April", "Maj", "Juni",
				      "Juli", "Augusti", "September", "Oktober", "November", "December"};
	static const char *wday[] = {"Må", "Ti", "On", "To", "Fr", "Lö", "Sö"};
	char num_buf[NUM_BUF_LEN];
	char title_buf[TITLE_BUF_LEN];
	GRect screen = GRect(0, 0, PBL_DISPLAY_WIDTH, PBL_DISPLAY_HEIGHT);
	GFont title_font = fonts_get_system_font(FONT_KEY_GOTHIC_24_BOLD);
	GFont week_font = fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD);
	GFont wday_font = fonts_get_system_font(FONT_KEY_GOTHIC_14);
	GFont day_font =  fonts_get_system_font(FONT_KEY_GOTHIC_14);
	GRect title_bounds = GRect(TITLE_OFFSET_X, TITLE_OFFSET_Y, PBL_DISPLAY_WIDTH-TITLE_OFFSET_X*2, TITLE_HEIGHT);
#ifdef PBL_PLATFORM_CHALK
	GRect year_bounds = GRect(TITLE_OFFSET_X, 144, PBL_DISPLAY_WIDTH-TITLE_OFFSET_X*2, TITLE_HEIGHT);
	char year_buf[TITLE_BUF_LEN];
#endif

	graphics_context_set_compositing_mode(ctx, GCompOpSet);

	graphics_context_set_fill_color(ctx, GColorWhite);
	graphics_fill_rect(ctx, screen, 0, GCornerNone);

#ifdef PBL_PLATFORM_BASALT
	snprintf(title_buf, TITLE_BUF_LEN, "%s - %d", mname[(int)s_view.curr_month], s_view.year);
#endif

#ifdef PBL_PLATFORM_CHALK
	snprintf(title_buf, TITLE_BUF_LEN, "%s", mname[(int)s_view.curr_month]);
	snprintf(year_buf, TITLE_BUF_LEN, "%d", s_view.year);
#endif

	graphics_context_set_text_color(ctx, GColorBlack);
	graphics_draw_text(ctx, title_buf, title_font, title_bounds, GTextOverflowModeWordWrap,
			   GTextAlignmentCenter, NULL);

#ifdef PBL_PLATFORM_CHALK
	graphics_draw_text(ctx, year_buf, title_font, year_bounds, GTextOverflowModeWordWrap,
			   GTextAlignmentCenter, NULL);
#endif

	for (int i = 0; i < NUM_DAYS; i++) {
		GRect wday_bounds = GRect(16*i+25+CAL_START_OFFSET_X, TITLE_HEIGHT+4+TITLE_OFFSET_Y, 20, 20);

		if (i == 6) /* Sunday */
			graphics_context_set_text_color(ctx, GColorRed);

		graphics_draw_text(ctx, wday[i], day_font, wday_bounds, GTextOverflowModeWordWrap,
				   GTextAlignmentCenter, NULL);

	}
	graphics_context_set_text_color(ctx, GColorBlack);

	for (int i = 0; i < NUM_WEEKS; i++) {
		GRect week_bounds = GRect(0+CAL_START_OFFSET_X, TITLE_HEIGHT+20+i*20+TITLE_OFFSET_Y, 20, 20);
		snprintf(num_buf, NUM_BUF_LEN, "%d", s_view.weeks[i]);
		graphics_draw_text(ctx, num_buf, week_font, week_bounds, GTextOverflowModeWordWrap,
				   GTextAlignmentRight, NULL);
	}

	for (int j = 0; j < NUM_WEEKS; j++) {
		for (int i = 0; i < NUM_DAYS; i++) {
			struct day d = s_view.days[i+j*NUM_DAYS];

			GRect wday_bounds = GRect(16*i+22+CAL_START_OFFSET_X, TITLE_HEIGHT+22+20*j+TITLE_OFFSET_Y, 16, 20);
			GRect bg_bounds =  GRect(16*i+24+CAL_START_OFFSET_X, TITLE_HEIGHT+22+20*j+TITLE_OFFSET_Y, 16, 20);
			GRect img_bounds =  GRect(16*i+24+CAL_START_OFFSET_X, 2+TITLE_HEIGHT+22+20*j+TITLE_OFFSET_Y, 16, 16);
			GBitmap *img = NULL;

			snprintf(num_buf, NUM_BUF_LEN, "%d", d.day);

			/* Font color */
			if ((d.flag & (DAY_RED|DAY_OFF)) == 0)
				graphics_context_set_text_color(ctx, GColorBlack);
			else if ((d.flag & DAY_OFF) == DAY_OFF)
				graphics_context_set_text_color(ctx, GColorMagenta);
			else if ((d.flag & DAY_RED) == DAY_RED)
				graphics_context_set_text_color(ctx, GColorRed);

			/* Backgrounds */
			if ((d.flag & DAY_TODAY) == DAY_TODAY) {
				graphics_context_set_fill_color(ctx, GColorRichBrilliantLavender);
				graphics_fill_rect(ctx, bg_bounds, 0, GCornerNone);
			} else if ((d.flag & DAY_MONTH1) == DAY_MONTH1) {
				graphics_context_set_fill_color(ctx, GColorMintGreen);
				graphics_fill_rect(ctx, bg_bounds, 0, GCornerNone);
			} else if ((d.flag & DAY_MONTH2) == DAY_MONTH2) {
				graphics_context_set_fill_color(ctx, GColorCeleste);
				graphics_fill_rect(ctx, bg_bounds, 0, GCornerNone);
			} else if ((d.flag & DAY_MONTH3) == DAY_MONTH3) {
				graphics_context_set_fill_color(ctx, GColorMintGreen);
				graphics_fill_rect(ctx, bg_bounds, 0, GCornerNone);
			}

			for (int j = 0; day2img[j].event != DAY_NORMAL; j++) {
				if ((d.flag & day2img[j].event) == day2img[j].event) {
					img = gbitmap_create_with_resource(day2img[j].res);
					break;
				}
			}

			if (img){
				graphics_draw_bitmap_in_rect(ctx, img, img_bounds);
				gbitmap_destroy(img);
			} else {
				graphics_draw_text(ctx, num_buf, wday_font, wday_bounds, GTextOverflowModeWordWrap,
						   GTextAlignmentRight, NULL);
			}
		}
	}
	kalender_timer_restart();
}

static void prv_select_click_handler(ClickRecognizerRef recognizer, void *context)
{
	s_now = time(NULL);
	generate_view(&s_view, s_now, 2);
	layer_mark_dirty(s_root_layer);
}

static void prv_up_click_handler(ClickRecognizerRef recognizer, void *context)
{
	s_now -= 24*60*60*7*2; /* Jump two weeks */
	generate_view(&s_view, s_now, 2);
	layer_mark_dirty(s_root_layer);
}

static void prv_down_click_handler(ClickRecognizerRef recognizer, void *context)
{
	s_now += 24*60*60*7*2; /* Jump two weeks */
	generate_view(&s_view, s_now, 2);
	layer_mark_dirty(s_root_layer);
}

static void prv_click_config_provider(void *context)
{
	window_single_click_subscribe(BUTTON_ID_SELECT, prv_select_click_handler);
	window_single_click_subscribe(BUTTON_ID_UP, prv_up_click_handler);
	window_single_click_subscribe(BUTTON_ID_DOWN, prv_down_click_handler);
}

static void prv_window_load(Window *window)
{
	s_root_layer = window_get_root_layer(window);
	layer_set_update_proc(s_root_layer, kalender_update_proc);

	generate_view(&s_view, s_now, 2);

	layer_mark_dirty(s_root_layer);
}

static void prv_window_unload(Window *window)
{
}

static void prv_init(void)
{
	struct tm *t;
	s_window = window_create();
	s_now = time(NULL);

	t = localtime(&s_now);
	s_today_day = t->tm_mday;
	s_today_mon = t->tm_mon;

	window_set_click_config_provider(s_window, prv_click_config_provider);
	window_set_window_handlers(s_window, (WindowHandlers) {
			.load = prv_window_load,
			.unload = prv_window_unload,
			});
	window_stack_push(s_window, false);
}

static void prv_deinit(void)
{
	window_destroy(s_window);
}

int main(void)
{
	prv_init();
	app_event_loop();
	prv_deinit();
}
