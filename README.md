# Kalender - Swedish Calender for pebble time

Kalender is a calender that focus primarily on Swedish holidays and events.
Plus I've added a cake symbol for family birthdays.
It's easy to adjust for your personal preferences by editing the "special_days" vector.

![alt text](screenshots/screenshot1.png)
![alt text](screenshots/screenshot2.png)
![alt text](screenshots/screenshot3.png)

Button up/down moves back/forth two weeks in time, while the select button returns to today.

Pebble Time Round support added November 2020. Classic Pebble is not supported.

## Licence

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

The icons are based on various images fond on the internet, converted to pebble
64 color palette, resized to 16x16 (or less in height). Sorry, lost track of them!
Except that many Swedish related icons comes from: [http://swemojis.com/](http://swemojis.com/)



